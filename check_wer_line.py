from jiwer import wer
import sys

try:
    reference = open(sys.argv[1],'r')
    hypothesis = open(sys.argv[2],'r')
    r=reference.readlines();
    h=hypothesis.readlines();

    for i in range(len(h)):
        error = wer(r[i], h[i])
        print("WER - line number "+str(i)+" : "+str(error))

    error_all = wer(r, h)
    print("WER_TOTAL : "+str(error_all))
except:
    print("Please using command \"python check_wer_line.py <reference.txt> <hypothesis.txt>\"")
    sys.exit()

